import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Candidato }    from './candidato';

import { AppCandidato } from './candidato.controller';

describe('AppCandidato', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppCandidato
      ],
      imports: [HttpModule, FormsModule],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppCandidato);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have initializated hero model`, async(() => {
    const fixture = TestBed.createComponent(AppCandidato);
    const app = fixture.debugElement.componentInstance;
    expect(app.model).toEqual(new Candidato(18, 'Dr IQ', 'Chuck Overstreet'));
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppCandidato);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Hero Form');
  }));
});
