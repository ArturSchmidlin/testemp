import { Component } from '@angular/core';
import { Candidato }    from './candidato';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'candidato-form',
  templateUrl: './candidato-form.component.html',
  styleUrls: ['./candidato.css']
})
export class AppCandidato {
  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];
  model = new Candidato(18, 'Dr IQ', 'Chuck Overstreet');
  submitted = false;
  onSubmit() { this.submitted = true; }
  newHero() {
    this.model = new Candidato(42, '', '');
  }
}